<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_nextbank2');

/** MySQL database username */
define('DB_USER', 'wp_nextbank');

/** MySQL database password */
define('DB_PASSWORD', 'nextbank2014');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[D-{yoE3df9-`|K5~S^*pEyt~F>GwC_7=+^4-B5IA?|<Wx/($]^5p(B)|{Ph0djb');
define('SECURE_AUTH_KEY',  '0?V,=pGVNT^ccF)lKZ:zj7;Z [j3!)Kc`Q6a!e]3x,@.8Ivph+QI^F_i7o8bG {1');
define('LOGGED_IN_KEY',    'U=>{(xL_P}Q%}s,+~]qNs%k#J?/>^T3]+)&2d3#E[T#w2+d2q$[VxwS:n?zj@S$K');
define('NONCE_KEY',        '4>P#0XW}I#L{C>u}GMoQM0)&vla^$S/d>F!kT{&{Tg1HbYU6AUC+~Vk2yC+M1(/Y');
define('AUTH_SALT',        'Y|5.01nti*zE[y^[L2}|$fg{%&QWoJ9A310%G-wK*ISU+aWB-%6g$QXs<=X?LEM|');
define('SECURE_AUTH_SALT', '+XtNBoPJ;N[?63D<%X[qKyn24=Mr7Lie?UWEferNLi1Lj0%~6F_@M|0M}~ tO-Ut');
define('LOGGED_IN_SALT',   'h4#?x/<R}V@bwM6G-Yl|&88Nf%|2TZT(Oz->ED*uOIN+@+h{8GXx}YX3?>zL*+rd');
define('NONCE_SALT',       'Fj}nu=+KrO++Nidi):rp1?V#o%/d0;sN02t}khYTZsJc9e+%6L58T|GRPqVxl!{-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
